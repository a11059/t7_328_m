A slightly modified version and configuration of the original m firmware for avr transtor testers by Markus Reschke. 
Available at  https://github.com/kubi48/TransistorTester-source.git
it works on the Chinese T7-LCR with atmega328 mcu, pictured below. 
The configuration is for hardware SPI and is modified to allow for one of the LCD pins being on a different port of the mcu. See pictures for the unit it has been tested on below.  
The repository also includes a circuit diagram which I think is reasonably accurate but may have errors.  
For further info and instructions see the original readme included in the repo.

![T7-LCR with Atmega328](/Photos/IMG_3682.JPG "Case")
![T7-LCR with Atmega328](/Photos/IMG_3684.JPG "PCB Back")
![T7-LCR with Atmega328](/Photos/IMG_3686.JPG "PCB Front")
